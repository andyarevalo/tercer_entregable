require('dotenv').config();
const express = require('express'); // import
const body_parser = require('body-parser');
const request_json = require('request-json');
const app = express();
const port = process.env.PORT || 3000; // puerto puede ser segun la variable o por 3000
const URL_BASE = '/techu/v2/';
const usersFile = require('./user.json');
const URL_mLab = 'https://api.mlab.com/api/1/databases/techu01db/collections/';
const apikey_mlab = 'apiKey=' + process.env.API_KEY_MLAB;
var newID = 0;

//Inicio
app.listen(port, function()
{
  console.log('Node JS escuchando en el puerto ' + port);
});


app.use(body_parser.json());

app.get(URL_BASE + 'users',
  function(req, res) {
    const http_client = request_json.createClient(URL_mLab);
      console.log("Cliente HTTP mLab creado.");
        const field_Param = 'f={"_id":0}&';
          http_client.get('users?' + field_Param + apikey_mlab,
            function(err, respuestamLab, body) {
        console.log('Error: ' + err);
        console.log('Respuesta mLab: ' + respuestamLab);
        console.log('Body: ' + body);
          var response = {};
            if(err) {
              response = {"msg" : "Error al recuperar users de mLab."}
                res.status(500);
        } else {
            if(body.length > 0) {
                response = body;
        } else {
              response = {"msg" : "Usuarios no encontrado."};
                res.status(404);
        }
        }
                res.send(response);
      });
});

//Peticiòn GET a un ùnico Usuario ID (Instanacia) busqueda por ID
  app.get (URL_BASE + 'users/:id',
    function(req,res){
      console.log ('GET /api/v2/users/:id');
      console.log(req.params.id);
        let id = req.params.id;
        let queryString = 'q={"id_user":' + id +  '}&';
        let fieldString = 'f={"_id":0}&';
        let  http_client = request_json.createClient(URL_mLab);

          http_client.get('users?' + queryString + fieldString + apikey_mlab,
              function(err, respuestamLab, body) {
                  console.log ("Respuesta Correcta.");
                  var response = {};
                    if (err){
                      response = {'msg':'Error en la peticiòn mLab'};
                        res.status (500);
                    }else{
                          if (body.length > 0){
                            response = body;
                      }else {
                          response = {'msg':'Usuario no encontrado'};
                          res.status(404);
                    }
                  }
                          res.send(response);
      });
    });

    // Cuentas del usuario con paràmetro ID
      app.get (URL_BASE + 'users/:id/accounts',
        function(req,res){
          console.log ('GET /api/v2/users/:id/accounts');
          console.log(req.params.id);
            let id = req.params.id;
            let queryString = 'q={"id_user":' + id +  '}&';  //tener en cuenta
            let fieldString = 'f={"_id":0}&';
            let lst_cuentas = 'f={"account":1, "_id":0}&';
            let  http_client = request_json.createClient(URL_mLab);

              http_client.get('users?' + queryString + lst_cuentas + fieldString + apikey_mlab, //se conceta con el Query

                  function(err, respuestamLab, body) {
                      console.log ("Listado de cuentas.");
                      var response = {};
                        if (err){
                          response = {'msg':'Error en la peticiòn mLab'};
                            res.status (500);
                        }else{
                              if (body.length > 0){
                                response = body;
                          }else {
                              response = {'msg':'No tiene cuentas'};
                              res.status(404);
                        }
                      }
                              res.send(response);
          });
        });

//Peticiòn POST a usersFile (creaciòn), no olvidar colocar data en el RAW
  app.post (URL_BASE + 'usersp',
    function(req,res){
        let  http_client = request_json.createClient(URL_mLab);
        console.log (req.body);
          http_client.get('users?' +  apikey_mlab, //se conceta con el Query
            function(error, respuestamLab, body) {
                let newID = body.length +1;
                  console.log("newID:" + newID);
                    var newUser = {
                          "id_user" : newID,
                          "first_name":req.body.first_name,
                          "last_name" :req.body.last_name,
                          "email" :req.body.email,
                          "password" :req.body.password
          };
          //se puede obviar pasarle la URL
            http_client.post(URL_mLab + 'users?' + apikey_mlab , newUser,
              function(error, respuestamLab, body){
                  console.log(body);
                        res.status(201);
                        res.send (body);
          });
    });
});

//Peticiòn PUT a usersFile (actualizaciòn), no olvidar colocar data en el RAW
app.put (URL_BASE + 'usersput/:id',
  function(req,res){
    //console.log ('GET /api/v2/users/:id');
    //console.log(req.params.id);
      let id = req.params.id;
      let queryString = 'q={"id_user":' + id +  '}&';
      let fieldString = 'f={"_id":0}&';
      let  http_client = request_json.createClient(URL_mLab);

        http_client.get('users?' + queryString + fieldString + apikey_mlab,
            function(error, respuestamLab, body) {
              var cambio = '{"set":' + JSON.stringify(req.body) +  '}';
                console.log (req.body);
                console.log(cambio);
        http_client.put(URL_mLab + 'users?' + queryString + apikey_mlab , JSON.parse(cambio),
              function(error, respuestamLab, body){
                  console.log("body:" + body);
                      res.send (body);
          });
      });
  });

  // Petición PUT con id de mLab (_id.$oid)
  app.put(URL_BASE + 'usersmLab/:id',
    function (req, res) {
      var id = req.params.id;
        let userBody = req.body;
          var queryString = 'q={"id_user":' + id + '}&';
            var httpClient = request_json.createClient(URL_mLab);

      httpClient.get('users?' + queryString + apikey_mlab,
        function(err, respuestaMLab, body){
          let response = body[0];
            console.log(body);          //Actualizo campos del usuario
              let updatedUser = {
            "id" : req.body.id,
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "email" : req.body.email,
            "password" : req.body.password
          };//Otra forma optimizada (para muchas propiedades)
          // var updatedUser = {};
          // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
          // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);          // PUT a mLab
          httpClient.put('users/' + response._id.$oid + '?' + apikey_mlab, updatedUser,
            function(err, respuestaMLab, body){
              var response = {};
              if(err) {
                  response = {
                    "msg" : "Error actualizando usuario."
                  }
                  res.status(500);
              } else {
                if(body.length > 0) {
                  response = body;
                } else {
                  response = {
                    "msg" : "Usuario actualizado correctamente."
                  }
                  res.status(404);
                }
              }
              res.send(response);
            });
        });
  });

//DELETE user with id
    app.delete(URL_BASE + "users/:id",
      function(req, res){
            console.log("entra al DELETE");
            console.log("req.params.id: "+ req.params.id);
          let id = req.params.id;
          let queryString = 'q={"id_user":' + id +  '}&';
          //let fieldString = 'f={"_id":0}&';
          let  http_client = request_json.createClient(URL_mLab);
            console.log('users?' + queryString);

        http_client.get('users?' + queryString +  apikey_mlab,
              function(error, respuestaMLab, body){
                  let respuesta = body[0];
                    console.log("body delete:"+ respuesta);

         http_client.delete(URL_mLab + 'users/' + respuesta._id.$oid + '?' + apikey_mlab,
                      function(error, respuestaMLab,body){
                 res.send(body);
         });
     });
 });

 // DELETE con PUT de mLab
     app.delete(URL_BASE + "usersdp/:id",
       function(req, res){
         let id = Number.parseInt(req.params.id);
            let queryString = 'q={"id_user":' + id + '}&';
         // Interpolación de expresiones
            let queryString2 = `q={"id_user":${id} }&`;
            let queryString3 = "q=" + JSON.stringify({"id_user": id})+ '&';

         let http_client = request_json.createClient(URL_mLab);
              console.log('users?' + queryString);

         httpClient.put("users?" + queryString + apikey_mlab, [{}],
             function(error, respuestaMLab,body) {
               var response = !error ? body : {"msg" : "Error borrando usuario."}
             res.send(response);
     });
});

//Method POST login
  app.post(URL_BASE + "login",
    function (req, res){
      console.log("POST a login");
          var email= req.body.email;
            var pass= req.body.password;
              var queryStringEmail='q={"email":"' + email + '"}&';
                var queryStringpass='q={"password":' + pass + '}&';
                  var http_client = request_json.createClient(URL_mLab);

    http_client.get('users?'+ queryStringEmail + apikey_mlab,

          function(error, respuestaMLab , body) {
            console.log("entro al body:" + body );
              var respuesta = body[0];
                console.log(respuesta);

          if(respuesta!=undefined){
            if (respuesta.password == pass) {
                console.log("Login Correcto");
                  var session={"logged":true};
                    var login = '{"$set":' + JSON.stringify(session) + '}';
                      console.log(URL_mLab+'?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab);

          http_client.put('users?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab, JSON.parse(login),
            function(errorP, respuestaMLabP, bodyP) {
                res.send(body[0]);
        });
            }
              else {
                res.send({"msg":"contraseña incorrecta"});
                    }
                  }else{
                      console.log("Email Incorrecto");
                        res.send({"msg": "email Incorrecto"});
                        }
          });
  });

  //Method POST logout
      app.post(URL_BASE + "logoutp",
        function (req, res){
            console.log("POST a logout");
              var email= req.body.email;
                var queryStringEmail='q={"email":"' + email + '"}&';
                  var http_client = request_json.createClient(URL_mLab);

    http_client.get('users?'+ queryStringEmail + apikey_mlab,
      function(error, respuestaMLab , body) {
        console.log("entro al get");
          var respuesta = body[0];
            console.log(respuesta);

      if(respuesta!=undefined){
        console.log("logout Correcto");
          var session={"logged":true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
              console.log(logout);

      http_client.put('users?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab, JSON.parse(logout),
        function(errorP, respuestaMLabP, bodyP) {
            res.send(body[0]);
      //res.send({"msg": "logout Exitoso"});
          });
              }else{
                  console.log("Error en logout");
                    res.send({"msg": "Error en logout"});
                    }
          });
});


  function writeUserDataToFile(data) {
      var fs = require('fs');
      var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
 function(err) {
        if(err) {
     console.log(err);
        } else {
     console.log("Datos escritos en 'users.json'.");
       }
}); }
